Rails.application.routes.draw do

  root 'home#index'

  mount_meter 'meter', config_path: '/widgets'

  resource :widgets do
    get '*name', to: 'widgets#proxy_analytics'
  end
end