require "meter/version"

module Meter

  class Engine < ::Rails::Engine

    isolate_namespace Meter

    config.before_configuration do |app|
      puts "before_configuration"
    end

  end

  class Configuration
    attr_accessor :mount_path
    attr_accessor :config_path
  end

  class << self
    attr_writer :configuration
  end

  module_function

  def configuration
    @configuration ||= Configuration.new
  end

  def configure
    yield(configuration)
  end

end
