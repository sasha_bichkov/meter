class ActionDispatch::Routing::Mapper

  def mount_meter(mount_path, options)
    mount Meter::Engine => mount_path

    Meter.configuration.mount_path = mount_path
    Meter.configuration.config_path = options[:config_path]
  end

end
