#= require meter/lib/core/model


class Meter.Models.LiveStreamSimpleModel extends Meter.Model

	defaults:
		title: ''
		rows: []
