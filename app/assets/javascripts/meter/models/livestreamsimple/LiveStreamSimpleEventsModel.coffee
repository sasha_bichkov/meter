#= require meter/lib/core/model


class Meter.Models.LiveStreamSimpleEventsModel extends Meter.Model

	defaults:
		filteredRows: []

	listen:
		'@filterModel change:filters': 'filterElements'

	initialize: (attributes, options) ->
		@url_path = options.url
		{ @filterModel } = options
		@setRows()


	filterElements: ->
		rows = @get 'rows'
		filters = @filterModel.get 'filters'
		filteredRows = @getRowsByFilters rows, filters if not _.isEmpty filters
		@setRows filteredRows


	url: ->
		params = from:'', to: ''
		"#{@url}?#{$.param params}"


	getRowsByFilters: (rows, filters) ->
		_.filter rows, (row) -> _.contains filters, row[0]


	setRows: (filteredRows) ->
		@set 'filteredRows', filteredRows || @get 'rows'
