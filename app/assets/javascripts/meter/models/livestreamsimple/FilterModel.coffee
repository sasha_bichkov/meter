#= require meter/lib/core/model


class Meter.Models.FilterModel extends Meter.Model

	defaults:
		filters: []
		categories: []

	initialize: (options, attributes) ->
		@categories = @getCategories @get 'rows'
		@set 'categories', @categories.sort()


	getCategories: (rows) ->
		_.uniq _.map rows, (r) -> r[0]
