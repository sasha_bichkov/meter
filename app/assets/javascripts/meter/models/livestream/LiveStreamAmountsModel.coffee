#= require meter/lib/core/model


class Meter.Models.LiveStreamAmountsModel extends Meter.Model

	listen:
		'@liveStreamEventsModel change': 'calcAmounts'

	initialize: (attributes, options) ->
		{ @liveStreamEventsModel } = options
		@calcAmounts()


	calcAmounts: ->
		amounts = @getAmounts()
		@set { amounts }


	getAmounts: (rows) ->
		rows = @liveStreamEventsModel.get 'filteredRows'
		rows = utils.transposeMatrix rows
		rows = @getOnlyNumericColumns rows
		_.map rows, (row) =>
			return '' unless row
			@getAmount row


	getOnlyNumericColumns: (rows) ->
		_.map rows, (row) =>
			result = _.find row, (r) => not utils.isNumber r
			row if _.isUndefined result


	getAmount: (row) ->
		utils.formatNum _.reduce row, (memo, num) =>
			memo + utils.toNum(num)
		, 0
