#= require meter/lib/core/view


class Meter.Views.LiveStreamSimpleEventsView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/simplelivestream/livestream-events']

	listen:
		'@model change': 'render'
