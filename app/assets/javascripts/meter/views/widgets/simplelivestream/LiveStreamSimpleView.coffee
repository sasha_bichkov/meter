#= require meter/lib/core/view


#= require meter/models/livestreamsimple/FilterModel
#= require ./FilterView

#= require meter/models/livestreamsimple/LiveStreamSimpleEventsModel
#= require ./LiveStreamSimpleEventsView


class Meter.Views.LiveStreamSimpleView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/simplelivestream/livestream-view']

	_render: ->
		@renderFilters()
		@renderEvents()


	renderFilters: ->
		rows = @model.get 'rows'
		@filterModel = new Meter.Models.FilterModel { rows }
		filterView = new Meter.Views.FilterView model: @filterModel
		@append '@livestream-filter', filterView


	renderEvents: ->
		[url, rows] = [@model.get('url'), @model.get('rows')]
		@liveStreamSimpleEventsModel = new Meter.Models.LiveStreamSimpleEventsModel { rows }, { url, @filterModel }
		@add '@livestream-events', Meter.Views.LiveStreamSimpleEventsView, model: @liveStreamSimpleEventsModel
