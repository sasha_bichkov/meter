#= require meter/lib/core/view

#= require ./charts/LineChartView
#= require ./charts/PieChartView
#= require ./charts/ColumnChartView
#= require ./charts/StackedColumnChartView
#= require ./charts/GroupStackedColumnChartView
#= require ./simplevalue/SimpleValueView
#= require ./livestream/LiveStreamView
#= require ./simplelivestream/LiveStreamSimpleView

#= require meter/views/QueryFormView
#= require meter/models/QueryFormModel

#= require ../SpinnerView


class Meter.Views.WidgetView extends Meter.View

	template: HandlebarsTemplates['widgets/widget']

	events:
		'submit @form': 'submit'
		'click @chart-update': 'refreshDataChart'

	listen:
		'@model sync': 'render'
		'@model error': 'render'
		'@model change:params': 'updatePath'
		'@rangeModel change': 'updatePath'


	initialize: (attributes, options) ->
		{ @rangeModel } = options


	_render: ->
		@renderContent()
		@renderQueryForm()
		@updatePath()


	renderContent: ->
		return unless @model.get 'alreadySync'
		ViewClass = @getView @model.get 'type'
		view = new ViewClass { @model, autoRender: false }
		@append '@chart-widget', view
		utils.timeoutCall =>
			view.render()
			@hideSpinner()
			@model.setSilent { isRendered: true }


	renderQueryForm: ->
		queries = @model.get 'queries'
		return unless queries
		unless @model.queryFormModel
			@model.queryFormModel = new Meter.Models.QueryFormModel form_params: queries
		@add '@queries-form', Meter.Views.QueryFormView, { model: @model.queryFormModel }


	submit: (e) ->
		e.preventDefault()
		@hideWidget()
		@showSpinner()
		@setForm()
		@model.fetch()


	setForm: ->
		form_params = @$('@form').serialize()
		@model.setFormParams form_params


	updatePath: ->
		params = @model.getParamsAsString()
		route = "#{meter_mount_path}/detail/#{@model.get('name')}?#{params}"
		link = @$('@app-route')
		link.attr('href', route)
		link.attr('data-route', route)


	refreshDataChart: ->
		@model.fetch()


	hideWidget: ->
		@$el.find('@chart-widget').hide()


	showSpinner: ->
		@$el.find('@spinner').show()


	hideSpinner: ->
		@$el.find('@spinner').hide()
		@$el.find('.chart__widget').show()


	getView: (type) =>
		{ Views } = Meter
		switch type
			when 'linechart'   then Views.LineChartView
			when 'piechart'    then Views.PieChartView
			when 'columnchart' then Views.ColumnChartView
			when 'simplevalue' then Views.SimpleValueView
			when 'livestream'  then Views.LiveStreamView
			when 'simplelivestream' then Views.LiveStreamSimpleView
			when 'stacked_column' then Views.StackedColumnChartView
			when 'group_stacked_column' then Views.GroupStackedColumnChartView
			else Views.LineChartView
