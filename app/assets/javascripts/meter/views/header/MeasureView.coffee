#= require meter/lib/core/view


class Meter.Views.MeasureView extends Meter.View

	template: HandlebarsTemplates['header/measure']

	listen:
		'@model change:measure': 'renderValue'

	events:
		'change': 'changeMeasure'

	_render: ->
		@renderValue()


	renderValue: ->
		measure = @model.get 'measure'
		@$("input[name=measure]").val [measure]


	changeMeasure: (event) ->
		@model.set measure: $(event.target).val()
