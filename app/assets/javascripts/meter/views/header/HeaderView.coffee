#= require meter/lib/core/view

#= require ./RangeView
#= require ./MeasureView
#= require ./PresetsView


class Meter.Views.HeaderView extends Meter.View

	template: HandlebarsTemplates['header/header']

	events:
		'click @show': 'toggleSettings'

	initialize: (options) ->
		{ @widgetsCollection } = options
		{ @rangeModel } = @widgetsCollection


	_render: ->
		@renderRange()
		@renderMeasure()
		@renderPresets()


	renderRange: ->
		@add '@range', Meter.Views.RangeView, model: @rangeModel


	renderMeasure: ->
		@add '@measure', Meter.Views.MeasureView, model: @rangeModel


	renderPresets: ->
		@add '@presets', Meter.Views.PresetsView, model: @rangeModel


	toggleSettings: ->
		@$('@settings').toggleClass 'hide'