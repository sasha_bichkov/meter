#= require meter/lib/core/view

#= require ./widgets/WidgetView


class Meter.Views.WidgetsView extends Meter.View

	listen:
		'@rangeModel change': 'refresh'
		'@collection reset': 'addWidgets'

	initialize: (attributes, options) ->
		{ @rangeModel } = options


	_render: ->
		@addWidgets()


	clearContainer: ->
		@$el.empty()


	addWidgets: ->
		@clearContainer()
		@collection.each (model) =>
			@addWidget model


	addWidget: (model) ->
		widgetView = new Meter.Views.WidgetView { model }, { @rangeModel }
		@append widgetView


	refresh: ->
		@updateUrl()
		@showSpinners()


	updateUrl: ->
		Backbone.history.navigate @getUrl()


	getUrl: ->
		path = url 'path'
		params = @rangeModel.getRangeParams()
		"#{path}?#{$.param params}"


	showSpinners: ->
		$('@spinner').show()
		$('.chart__widget').hide()
