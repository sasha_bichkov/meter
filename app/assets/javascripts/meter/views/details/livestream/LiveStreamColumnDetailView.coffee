#= require meter/lib/core/view


class Meter.Views.LiveStreamColumnDetailView extends Meter.View

	tagName: 'td'

	template: HandlebarsTemplates['details/livestream/livestream-column']

	listen:
		'@model change': 'render'

	events:
		'click @sort': 'setSort'
		'click @livestream-select-option': 'setFilter'

	setSort: ->
		@model.setNextSortStatus()


	setFilter: (e) ->
		[id, filterName] = @getData(e)
		@model.setFilter id, filterName


	getData: (e) ->
		$el = $(e.target)
		[$el.data('id'), $el.data('value')]
