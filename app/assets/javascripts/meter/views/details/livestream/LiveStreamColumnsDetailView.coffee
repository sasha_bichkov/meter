#= require meter/lib/core/view

#= require ./LiveStreamColumnDetailView


class Meter.Views.LiveStreamColumnsDetailView extends Meter.View

	template: HandlebarsTemplates['details/livestream/livestream-header-view']

	listen:
		'@collection change': 'render'

	_render: ->
		@renderColumns()


	renderColumns: ->
		@collection.each (model) =>
			@renderColumn model


	renderColumn: (model) ->
		view = new Meter.Views.LiveStreamColumnDetailView { model }
		@append '@livestream-header', view