#= require meter/lib/core/view

#= require ./LineChartView
#= require ./PieChartView
#= require ./ColumnChartView
#= require ./SimpleValueView
#= require ./StackedColumnChartView
#= require ./GroupStackedColumnChartView
#= require ./livestream/LiveStreamDetailView
#= require ./simplelivestream/LiveStreamSimpleView

#= require meter/views/QueryFormView
#= require meter/models/QueryFormModel


class Meter.Views.DetailView extends Meter.View

	template: HandlebarsTemplates['details/widget']

	events:
		'submit @form': 'submit'
		'click @chart-update': 'refreshDataChart'

	listen:
		'@rangeModel change': 'refresh'
		'@collection reset': 'setModel'

	initialize: (attributes, options) ->
		{ @name, @rangeModel } = options


	_render: ->
		if @model
			@renderWidget()
			@renderQueryForm()
			@updatePath()


	refresh: ->
		@updateUrl()
		@resetChart()


	resetChart: ->
		@resetSync()
		@clearContainer()
		@render()


	setModel: ->
		@model = @getModelByName @name
		if @model
			unless @model.queryFormModel
				@model.setQueryModel()
				@model.loadParamsFromUrl()

			@listenTo @model, 'sync', @render
			@listenTo @model, 'error', @render
			@listenTo @model, 'change:params', @updatePath


	renderWidget: ->
		if @model.get 'alreadySync'
			View = @getView @model.get 'type'
			widget = new View { @model, autoRender: false }
			@append '@chart-widget', widget
			utils.timeoutCall =>
				widget.render()
				@hideSpinner()


	renderQueryForm: ->
		return unless @model.queryFormModel
		@add '@queries-form', Meter.Views.QueryFormView, { model: @model.queryFormModel }


	resetSync: ->
		@model.setSilent { alreadySync: false } if @model


	updateUrl: ->
		return unless @model
		params = @model.getParamsAsString()
		route = "#{url('path')}?#{params}"
		Backbone.history.navigate route


	updatePath: ->
		rangeParams = $.param @model.getRangeParams()
		route = "#{meter_mount_path}?#{rangeParams}"
		link = @$('@app-route-goback')
		link.attr('href', route)
		link.attr('data-route', route)


	getModelByName: (name) ->
		@collection.findWhere { name }


	getView: (type) ->
		Views = Meter.Views
		switch type
			when 'linechart'   then Views.LineChartDetailView
			when 'piechart'    then Views.PieChartDetailView
			when 'columnchart' then Views.ColumnChartDetailView
			when 'simplevalue' then Views.SimpleValueDetailView
			when 'livestream'  then Views.LiveStreamDetailView
			when 'simplelivestream' then Views.LiveStreamSimpleDetailView
			when 'stacked_column' then Views.StackedColumnChartDetailView
			when 'group_stacked_column' then Views.GroupStackedColumnChartDetailView
			else Views.LineChartDetailView


	submit: (e) ->
		e.preventDefault()
		@hideWidget()
		@showSpinner()
		@setForm()
		@updateUrl()
		@model.fetch()


	setForm: ->
		form_params = @$('@form').serialize()
		@model.setFormParams form_params


	refreshDataChart: ->
		@model.fetch()


	clearContainer: ->
		@$el.empty()


	hideWidget: ->
		@$el.find('@chart-widget').hide()


	showSpinner: ->
		@$el.find('@spinner').show()


	hideSpinner: ->
		@$el.find('@spinner').hide()
