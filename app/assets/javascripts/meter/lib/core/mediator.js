Meter.mediator = mediator = {};
mediator.subscribe   = mediator.sub   = Backbone.Events.on;
mediator.unsubscribe = mediator.unsub = Backbone.Events.off;
mediator.publish     = mediator.pub   = Backbone.Events.trigger;
