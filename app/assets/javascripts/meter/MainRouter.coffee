#= require ./lib/core/router


class Meter.MainRouter extends Meter.Router

	routes:
		"#{meter_mount_path}(/)": 'dashboard'
		"#{meter_mount_path}/detail/:name": 'chart'

	initialize: (attributes, options) ->
		{ @meterView } = options


	dashboard: ->
		@meterView.switchDashboard()


	chart: (name) ->
		@meterView.switchWidget name
